<?php

/**
 * @file
 * Primary hook implementations for Aframe Extra.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Component\Utility\Html;

/**
 * Implements hook_help().
 */
function aframe_extra_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.aframe_extra':
      $text = file_get_contents(__DIR__ . '/README.md');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . Html::escape($text) . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}

/**
 * Implements hook_library_info_build().
 */
function aframe_extra_library_info_build() {
  // Get the latest version of aframe.
  $aframe_components_service = \Drupal::service('aframe_extra.component.discovery');
  $aframe_components = $aframe_components_service->aframeScanComponents();
  $libs = [];
  foreach ($aframe_components as $component) {
    $library_name = str_replace('-', '_', $component);
    $files = $aframe_components_service->aframeGetComponentFiles($component);
    $libs[$library_name] = $files;
  }
  return $libs;
}
