<?php

namespace Drupal\aframe_extra\Services;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Class AFrameCompnentDiscovery.
 *
 * @package Drupal\aframe_extra
 */
class AFrameComponentDiscovery {

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Path to the aframe components folder.
   *
   * @var string
   */
  protected $aframeComponentsPath;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory) {
    $this->aframeComponentsPath = DRUPAL_ROOT . base_path() . 'libraries/aframecomponent/';
    $this->logger = $logger_factory->get('aframe_extra');
  }

  /**
   * Function to get the path to the library file of a component.
   *
   * @param string $component_name
   *   The component name.
   *
   * @return string
   *   The path to the component name library.
   */
  public function getAframeComponentPath($component_name) {
    $aframe_component_path = $this->aframeComponentsPath . $component_name . '/dist/';
    return $aframe_component_path;
  }

  /**
   * Function to get the different versions of the aframe library.
   */
  public function aframeScanComponents() {
    $aframe_components = [];
    $handle = @opendir($this->aframeComponentsPath) or $this->logger->notice("Unable to open " . $this->aframeComponentsPath);
    while ($entry = @readdir($handle)) {
      if (!in_array($entry, ['.', '..'])) {
        $aframe_components[] = $entry;
      }
    }
    closedir($handle);

    return $aframe_components;
  }

  /**
   * Function to get the js file from a component.
   */
  public function aframeGetComponentFiles($component_name) {
    $aframe_component_files = [];
    $component_path = $this->getAframeComponentPath($component_name);
    $handle = @opendir($component_path) or $this->logger->notice("Unable to open " . $component_path);
    while ($entry = @readdir($handle)) {
      $matches = [];
      $file_path = $component_path . $entry;
      if (is_file($file_path) && preg_match('/(.*)\.min\.js$/i', $entry, $matches)) {
        $relative_path = base_path() . 'libraries/aframecomponent/' . $component_name . '/dist/' . $entry;
        $aframe_component_files[$relative_path] = [];
      }
    }
    closedir($handle);

    return [
      'js' => $aframe_component_files,
    ];
  }

}
